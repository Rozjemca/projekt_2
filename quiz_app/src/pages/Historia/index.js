import Quiz from '../../components/Quiz';
import icon from '../../assets/historia_ikona.svg';

const Historia = () => {
    return(
        <div className="quiz Historia">
            <span class="quiz__title">QUIZ</span>
            <div class="quiz__scarf--Historia">WYBRANA KATEGORIA:</div>
            <img src={icon} alt="icon" className="quiz__icon"></img>
            <Quiz type="Historia"/>
        </div>
    )
}
export default Historia;