import Quiz from '../../components/Quiz';
import icon from '../../assets/programowanie_ikona.svg';

const Programowanie = () => {
    return(
        <div className="quiz Programowanie">
            <span className="quiz__title">QUIZ</span>
            <div className="quiz__scarf--Programowanie">WYBRANA KATEGORIA:</div>
            <img alt="icon" src={icon} className="quiz__icon"></img>
            <Quiz type="Programowanie"/>
        </div>
    )
}
export default Programowanie;