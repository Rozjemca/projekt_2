import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";
import Technologia from '../Technologia';
import Programowanie from '../Programowanie';
import Motoryzacja from '../Motoryzacja';
import Kultura from '../Kultura';
import Historia from '../Historia';
import technologia_ikona from '../../assets/technologia_ikona.svg';
import programowanie_ikona from '../../assets/programowanie_ikona.svg';
import motoryzacja_ikona from '../../assets/motoryzacja_ikona.svg';
import kultura_ikona from '../../assets/kultura_ikona.svg';
import historia_ikona from '../../assets/historia_ikona.svg';

const MainMenu = () => {
    return(
        <div> 
            <Router>
            <div className="menu">
                    <Link to="/technologia">
                        <div className="menu__item">
                            <img alt="icon" src={technologia_ikona}></img>
                            <figcaption>Technologia</figcaption>
                        </div>
                    </Link>
                    <Link to="/programowanie">
                        <div className="menu__item">
                            <img alt="icon" src={programowanie_ikona}></img>
                            <figcaption>Programowanie</figcaption>
                        </div>
                    </Link>
                    <Link to="/motoryzacja">
                        <div className="menu__item">
                            <img alt="icon" src={motoryzacja_ikona}></img>
                            <figcaption>Motoryzacja</figcaption>
                        </div>
                    </Link>
                    <Link to="/kultura">
                        <div className="menu__item">
                            <img alt="icon" src={kultura_ikona}></img>
                            <figcaption>Kultura</figcaption>
                        </div>
                    </Link>
                    <Link to="/historia">
                        <div className="menu__item">
                            <img alt="icon" src={historia_ikona}></img>
                            <figcaption>Historia</figcaption>
                        </div>
                    </Link>
            </div>

            <Switch>
                <Route exact path="/technologia">
                    <Technologia />
                </Route>
                <Route exact path="/programowanie">
                    <Programowanie />
                </Route>
                <Route exact path="/motoryzacja">
                    <Motoryzacja />
                </Route>
                <Route exact path="/kultura">
                    <Kultura />
                </Route>
                <Route exact path="/historia">
                    <Historia />
                </Route>
            </Switch>
            </Router>
        </div>
    )
}
export default MainMenu;