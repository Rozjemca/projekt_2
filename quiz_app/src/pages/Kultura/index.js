import Quiz from '../../components/Quiz';
import icon from '../../assets/kultura_ikona.svg';

const Kultura = () => {
    return(
        <div className="quiz Kultura">
            <span class="quiz__title">QUIZ</span>
            <div class="quiz__scarf--Kultura">WYBRANA KATEGORIA:</div>
            <img src={icon} alt="icon" className="quiz__icon"></img>
            <Quiz type="Kultura"/>
        </div>
    )
}
export default Kultura;