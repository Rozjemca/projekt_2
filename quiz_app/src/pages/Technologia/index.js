import Quiz from '../../components/Quiz';
import icon from '../../assets/technologia_ikona.svg';

const Technologia = () => {
    return(
        <div className="quiz Technologia">
            <span className="quiz__title">QUIZ</span>
            <div className="quiz__scarf--Technologia">WYBRANA KATEGORIA:</div>
            <img alt="icon" src={icon} className="quiz__icon"></img>
            <Quiz type="Technologia"/>
        </div>
    )
}
export default Technologia;