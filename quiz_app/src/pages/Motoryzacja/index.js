import Quiz from '../../components/Quiz';
import icon from '../../assets/motoryzacja_ikona.svg';

const Motoryzacja = () => {
    return(
        <div className="quiz Motoryzacja">
            <span class="quiz__title">QUIZ</span>
            <div class="quiz__scarf--Motoryzacja">WYBRANA KATEGORIA:</div>
            <img alt="icon" src={icon} className="quiz__icon"></img>
            <Quiz type="Motoryzacja" />
        </div>
    )
}
export default Motoryzacja;