import './App.css';
import MainMenu from './pages/MainMenu';
import '../src/styles/themes/default/theme.scss';

function App() {
  return (
    <div className="main">
      <a href={'./'}><p className="main__icon">Q</p></a>
      <p className="main__menu-label">QUIZ</p>
      <MainMenu />
    </div>
  );
}

export default App;
