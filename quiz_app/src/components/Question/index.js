import GetQuestion from '../GetQuestion';

const Question = ({type}) => {
    const typeClass = "quiz__scarf " + "quiz__scarf--"+type;
    const mainClass = "quiz " + type;
    return (
        <div className={mainClass}>
            <p className="quiz__title">QUIZ</p>
            <span className={typeClass}>Select correct answer</span>
            <GetQuestion type={type} />
        </div>
    );
}
 
export default Question;