import Motoryzacja from '../../assets/motoryzacja_ikona.svg';
import Technologia from '../../assets/technologia_ikona.svg';
import Programowanie from '../../assets/programowanie_ikona.svg';
import Kultura from '../../assets/kultura_ikona.svg';
import Historia from '../../assets/historia_ikona.svg';
import button from '../../assets/button_strzałka_plus.svg';

const Results = ({score, type}) => {
    const mainClass = "quiz " + type;
    const smallscarf = "quiz__small-scarf";
    const circle = "quiz__small-scarf--circle quiz__small-scarf--circle--"+type;
    const smally = "quiz__small-scarf--"+type;
    const buttonClass="quiz__main--button--"+type;
    const pocketMenuClass = "quiz__pocket-menu--item quiz__pocket-menu--item--"+type;

    return ( 
        <div className={mainClass}>
            <div className="quiz__pocket-menu">
                <p>Wybierz inną <br /> kategorię:</p>
                <div className={pocketMenuClass}></div>
                <div className={pocketMenuClass}></div>
                <div className={pocketMenuClass}></div>
                <div className={pocketMenuClass}></div>
            </div>
            <div className="quiz__main">
                <p className="quiz__title">QUIZ</p>
                {
                    type==="Programowanie" ? (<img alt="icon" src={Programowanie} className="quiz__icon"></img>)
                    :
                    type==="Technologia" ? (<img alt="icon" src={Technologia} className="quiz__icon"></img>)
                    :
                    type==="Motoryzacja" ? (<img alt="icon" src={Motoryzacja} className="quiz__icon"></img>)
                    :
                    type==="Historia" ? (<img alt="icon" src={Historia} className="quiz__icon"></img>)
                    :
                    type==="Kultura" ? (<img alt="icon" src={Kultura} className="quiz__icon"></img>)
                    :
                    <div></div>
                }
                <div className="quiz__main--text" >{type}</div>
                
                <div className={smallscarf}>
                    <div className={smally}>Twój wynik</div>
                    <div className={circle}>{score}/10</div>
                </div>

                <div className={buttonClass} style={{marginTop: 40+'px'}}>
                    <span>Powtórz Quiz</span>
                    <img alt="button" src={button}></img>
                </div>
            </div>
        </div>
     );
}
 
export default Results;