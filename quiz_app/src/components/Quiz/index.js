import button from '../../assets/button_strzałka_plus.svg';
import Question from '../Question';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";


const Quiz = ({type}) => {
    const buttonClass="quiz__main--button--"+type;
    return(
        <Router>
        <div className="quiz__main">
            <div className="quiz__main--text">{type}</div>
            <Link to="/question">
                <div className={buttonClass}>
                    <span>Rozpocznij</span>
                    <img alt="button" src={button}></img>
                </div>
            </Link>
        </div>

        <Switch>
            <Route exact path="/question">
                <Question type={type} />
            </Route>
        </Switch>
        </Router>
    )
}
export default Quiz;