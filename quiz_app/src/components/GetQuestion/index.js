import {useState, useEffect } from 'react';
import Results from '../Results';

const GetQuestion = ( {type} ) => {
    const [classname, setClassname] = useState("question__answer question__answer--"+type);
    const [question, setQuestion] = useState([]);
    const [data, setData] = useState([]);
    const [cout, setCout] = useState(1);
    const [ans, setAns] = useState('');
    const [score, setScore] = useState(0);

    const getData=()=>{
        fetch('questions.json'
        ,{
            headers : { 
            'Content-Type': 'application/json',
            'Accept': 'application/json'
            }
        })
          .then(function(response){
            return response.json()
          })
          .then(function(myJson) {
            setData(myJson)
          });
      }
      useEffect(()=>{
        getData()
      },[])

      const nextQuestion = (val, corr) => {
        if(val === corr) {
            setClassname("question__answer question__answer--"+type+" question__answer--right");
            setScore(score+1);
        }
        else {  
            setClassname("question__answer question__answer--"+type+" question__answer--wrong");
        }

        setTimeout(function() { //Start the timer
            setCout(cout+1);
            setClassname("question__answer question__answer--"+type);
        }.bind(this), 700)
    }

    return ( 
        <div>
            {data && data.length>0 && data.map((item)=>
            item.type===type & item.id==cout ?
            <div>
                <div className="question__quest" key={item.index}> {item.question} </div>
                <div className="question__main">
                    <button className={classname} key={item.id+10} onClick={() => nextQuestion(item.answer_1, item.correct_answer)}> {item.answer_1} </button>
                    <button className={classname} key={item.id+11} onClick={() => nextQuestion(item.answer_2, item.correct_answer)}> {item.answer_2} </button>
                    <button className={classname} key={item.id+12} onClick={() => nextQuestion(item.answer_3, item.correct_answer)}> {item.answer_3} </button>
                    <button className={classname} key={item.id+13} onClick={() => nextQuestion(item.answer_4, item.correct_answer)}> {item.answer_4} </button>
                    <button className={classname} key={item.id+14} onClick={() => nextQuestion(item.answer_5, item.correct_answer)}> {item.answer_5} </button>
                </div>
            </div>
            :
            <></>
            )}
            {cout > 10 ?
                <Results score={score} type={type} />
            :
                <div></div>
            }  
        </div> 
    );
}
 
export default GetQuestion;